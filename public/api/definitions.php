<?php
require_once __DIR__.'/../../config.inc.php';

// Ne pas formatter l’HTML
header('Content-type: text/plain');

$monMot = filter_input(INPUT_GET, 'word', FILTER_SANITIZE_STRING);

// Test si il n'y a pas de mot entré
if (!$monMot) {
    http_response_code(400);
    die('Missing parameter word');
}

$urlPart1 = "https://fr.wiktionary.org/w/api.php?action=parse&format=json&page=";
// Enregistre les 2 possibilités d'url (le mot tel qu'entré par l'utilisateur et la meme chaine uniquement en minuscules

// Envoi l'url dans la fonction qui appelle l'API
$tempResponse = callAPI($urlPart1 . $monMot);
// Recupere la réponse et décode le Json
$response = json_decode($tempResponse, true);

// Si une erreur existe dans la réponse on teste avec l'autre possibilité d'url: en miniscules
if (isset($response['error'])) {
  $monMot = mb_strtolower($monMot);

  $tempResponse = callAPI($urlPart1 . $monMot);
  $response = json_decode($tempResponse, true);

  // Si l'erreur est toujours présente on tue le process et affiche un message d'erreur
  if (isset($response['error'])){
    http_response_code(404);
    die('Word not found');
  }
}

// Si tout c'est bien passé on récupere la section qui contient le texte
$content = $response['parse']['text']['*'];
// Affiche le contenu récupéré

echo json_encode([
  'word' => $monMot,
  'definition' => $content,
]);

/**
 * Fonction qui reçoit un URL et récupère en Json les données stockées à l'adresse
 * @param string $url de l'API avec tous les paramètres désirés
 * @return string Résultat de la requête
 */
function callAPI($url)
{
    // get cURL resource
    $ch = curl_init();
    
    // set url
    curl_setopt($ch, CURLOPT_URL, $url);
    
    // set method
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    
    // return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    
    // send the request and save response to $response
    $response = curl_exec($ch);
    
    // stop if fails
    if (!$response) {
      die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }
    
    // close curl resource to free up system resources 
    curl_close($ch);
    
    // Retourne le résultat
    return $response;
}
