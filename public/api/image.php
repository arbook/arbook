<?php
/**
 * Détecte les mots dans une image et les renvoie
 */

require_once __DIR__.'/../../config.inc.php';
use ARBook\WordsDetector;

// Récupérer une image depuis un fichier (pour le debug)
// $image = file_get_contents(__DIR__.'/figure-65.png');

// Récupérer l’image téléversée en base64
$input = json_decode(@file_get_contents('php://input'), true);
if ($input && $input['imageData']) {
    $dataUri = $input['imageData'];
    $dataUri = str_replace('data:image/png;base64,', '', $dataUri);
    $dataUri = str_replace(' ', '+', $dataUri);
    $image = base64_decode($dataUri);

    $words = WordsDetector::detectWordsIn($image);

    header('Content-type: application/json');
    echo json_encode([
        'words' => $words,
    ]);
}