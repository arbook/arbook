<?php define('DOMAIN', (isset($_SERVER['HTTPS']) ? 'https://' : 'http://').$_SERVER['HTTP_HOST']); ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Permet d’ajouter la webapp à l’écran d’accueil sur iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="apple-touch-icon" href="<?= DOMAIN ?>/app/apple-touch-icon.png">

    <!--
        Permet de faire en sorte que par défaut, tout les liens relatifs de la page mènent vers
        le Wiktionnaire. Cela permet d’avoir un lien <a href="Lune">Lune</a> qui mènera vers
        https://fr.wiktionary.org/wiki/lune au lieu de https://arbook.club/app/Lune.
        Pour cette raison, toutes les URLs qui mènent vers arbook.club doivent être absolues (en entier).
    -->
    <base href="https://fr.wiktionary.org/wiki/" target="_blank">

    <title>ARBook</title>

    <link rel="stylesheet" href="<?= DOMAIN ?>/app/style.css?<?= time() ?>">
</head>
<body>
    <!-- Affichage en temps réel du flux vidéo -->
    <video id="video" autoplay playsinline controls></video>

    <!-- Div qui contient la photo figée et la définition -->
    <div class="photo-overlay">
        <div class="photo">
            <div class="loader"></div> <!-- Indicateur de chargement -->
            <canvas></canvas> <!-- Affichage de la photo figée -->
        </div>

        <!-- Affichage de la définition -->
        <div class="definition-panel">
            <header>
                <span class="definition-title">{MOT}</span>
                <button class="button-close">×</button>
            </header>

            <main class="definition-content">
                <p class="definition-text">{DÉFINITION}</p>
                <footer>
                    <a id="href" href='https://fr.wiktionary.org/wiki/' >Source : Wiktionnaire</a>
                </footer>
            </main>
        </div>
    </div>

    <!-- Sentry (monitoring des erreurs côté client) -->
    <script src="https://browser.sentry-cdn.com/4.2.3/bundle.min.js" crossorigin="anonymous"></script>
    <script>
    	Sentry.init({ dsn: 'https://728b116254194fbc9b88f991e4bb110c@sentry.io/1312748' });
    </script>

    <script src="<?= DOMAIN ?>/app/index.js?<?= time() ?>"></script>
</body>
</html>
