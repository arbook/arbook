/* global fetch */
'use strict';

const domain = `${window.location.protocol}//${window.location.host}`;

const video = document.querySelector('video');

// Récupérer l'autorisation caméra
if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia && !window.mockImage) {
    navigator.mediaDevices.getUserMedia({
        audio: false,
        video: {
            width: 1280,
            height: 720,
            facingMode: 'environment', // Caméra arrière
        },
    })
        .then((stream) => {
            video.srcObject = stream; // Afficher la sortie de la caméra dans l'élément <video>
            video.play();
        });
}

const photoOverlay = document.querySelector('.photo-overlay'); // Affichage de la photo + définition
const definitionPanel = document.querySelector('.definition-panel'); // Affichage de la définition

// Réagir lorsqu’on appuie sur la vidéo
video.addEventListener('mousedown', (event) => {
    onVideoTouch(event.clientX, event.clientY);
});

// La taille du canevas où est affichée l’image figée est plus
// petite que celle de l’image originale pour réduire les données
// utilisées lors du transfert du serveur.
// Taille du canevas = taille de l’image / {imageRatio}
const imageRatio = 2;

/**
 * Lorsqu'on clique sur la vidéo
 * @param {Number} Coordonnée X du toucher sur l’élément <video>
 * @param {Number} Coordonnée Y du toucher sur l’élément <video>
 * @returns void
 */
function onVideoTouch(touchX, touchY) {
    const loader = document.querySelector('.loader'); // Image "chargement en cours"
    loader.classList.remove('hidden'); // Afficher le loader
    photoOverlay.classList.add('visible'); // Affiche l'overlay

    // Supprimer l'ancien canvas
    document.querySelector('canvas').remove();

    // Créer un nouveau canvas et l’ajouter à la page
    const canvas = document.createElement('canvas');
    canvas.setAttribute('width', video.videoWidth / imageRatio);
    canvas.setAttribute('height', video.videoHeight / imageRatio);
    const photo = document.querySelector('.photo');
    photo.appendChild(canvas);

    // Dessiner l’image sur le canevas
    const ctx = canvas.getContext('2d');
    ctx.drawImage(video, 0, 0, video.videoWidth / imageRatio, video.videoHeight / imageRatio);

    // Transformer les coordonnées sur l’élément <video> en coordonnées sur l’image
    const videoElementBounds = video.getBoundingClientRect();
    const touchOnVideo = viewPositionToCameraPosition(
        { x: touchX, y: touchY },
        videoElementBounds.width,
        videoElementBounds.height,
        video.videoWidth,
        video.videoHeight
    );
    touchOnVideo.x /= imageRatio; // Appliquer le ratio
    touchOnVideo.y /= imageRatio;

    // DEBUG : Afficher la position du toucher (dessiner une croix à son emplacement)
    /*
        const debugCrossLength = 10;
        ctx.beginPath();
        ctx.moveTo(touchOnVideo.x, touchOnVideo.y - debugCrossLength);
        ctx.lineTo(touchOnVideo.x, touchOnVideo.y + debugCrossLength);
        ctx.lineWidth = 2;
        ctx.strokeStyle = "orange";
        ctx.stroke();
        ctx.closePath();

        ctx.beginPath();
        ctx.moveTo(touchOnVideo.x - debugCrossLength, touchOnVideo.y);
        ctx.lineTo(touchOnVideo.x + debugCrossLength, touchOnVideo.y);
        ctx.lineWidth = 2;
        ctx.strokeStyle = "orange";
        ctx.stroke();
        ctx.closePath();
    */

    // Transformer l'image capturée en data URL (pour l’envoyer au serveur)
    const imageData = window.mockImage || canvas.toDataURL('image/png');

    // Récupérer les mots contenus dans l’image
    fetch(`${domain}/api/image.php`, {
        method: 'POST',
        body: JSON.stringify({
            imageData,
        }),
    })
        .then(response => response.json())
        .then((result) => {
            // Aucun mot trouvé ? On affiche un message et on masque l’image
            if (result.words.length === 0) {
                alert('Aucun mot trouvé');
                photoOverlay.classList.remove('visible');
                photoOverlay.classList.remove('with-definition');
                definitionPanel.classList.remove('visible');
                return;
            }

            // Parmi tous les mots trouvés, récupérer le bon mot
            const word = result.words
                .filter(word => word.value.split(' ').length === 1) // Uniquement les détections de mots sans espace (pas plusieurs mots)
                .map(word => ({ // Calculer pour chaque mot sa distance du toucher
                    distance: Math.hypot(
                        touchOnVideo.x - (word.center.x),
                        touchOnVideo.y - (word.center.y),
                    ),
                    word
                }))
                .sort((word1, word2) => word1.distance - word2.distance) // Trier par distance
            [0].word; // Prendre le premier mot

            let {
                corners,
                value, // Mot
            } = word;

            /*
                Affichage sur l’écran du mot détecté
            */
            // Trace une forme à partir des coordonnées des coins
            // du mots renvoyées par l’API Google Vision
            ctx.beginPath();
            ctx.moveTo(corners[0].x, corners[0].y);
            for (let i = 1; i < corners.length; i++) {
                ctx.lineTo(corners[i].x, corners[i].y);
            }
            ctx.closePath();

            // L’afficher sous la forme d’un rectangle noir
            ctx.lineWidth = 2;
            ctx.strokeStyle = '#333333';
            ctx.stroke();

            /*
                Obtenir la définition du dictionnaire
            */

            // Retirer la ponctuation avant et après le mot
            const match = value.match(/\b(.*)\b/g);
            if (match !== null) {
                value = match[0];
            }

            // Cacher le loader
            loader.classList.add('hidden');

            fetch(`${domain}/api/definitions.php?word=${value}`)
                .then((response) => response.json())
                .then((data) => {
                    const def = data.definition;
                    const word = data.word;

                    // Afficher la définition chargée depuis l’API
                    document.querySelector('.definition-title').innerHTML = word;
                    document.getElementById('href').href = word;
                    document.querySelector('.definition-text').innerHTML = def;

                    // Démasquer l’élément d’affichage de la définition
                    photoOverlay.classList.add('with-definition');
                    definitionPanel.classList.add('visible');
                });
        });
}

// Cacher le photo overlay quand on appuie dessus
// (Pour capturer un nouveau mot après une première recherche)
photoOverlay.addEventListener('click', () => {
    photoOverlay.classList.remove('visible');
    photoOverlay.classList.remove('with-definition');
    definitionPanel.classList.remove('visible');
});

// Remove controls attribute on the <video> tag
// (should exist in the first place to make it work on iOS Safari)

// Ce hack permet de faire fonctionner AR Book sur Safari sous iOS (iPhone/iPad).
// Par défaut, sous ce navigateur, les vidéos qui n’ont aucun contrôle (play/pause…)
// ne se lancent pas automatiquement. Pour y remédier, on met les contrôles sur la vidéo
// lors du chargement de la page, puis on les retire immédiatement. Cela permet au flux
// vidéo de se lancer.
setTimeout(() => {
    video.removeAttribute('controls');
});

/**
 * Transform a (x, y) coordinates pair on the <video> view
 * to a (x, y) coordinates pair on the camera
 * @todo (Currently it only works on Nicolas’s phone in portrait mode)
 */

/**
 * Cette fonction transforme une paire de coordonnées (x, y) relative à
 * l’élément <video> en paire de coordonnées (x, y) relative à l’image.
 *
 * Sur l’élément vidéo, le flux vidéo s’affiche, mais pas forcément
 * à la même taille. Il est aussi rogné sur les côtés pour que tout
 * l’écran soit rempli par le flux. Lorsque l’utilisateur touche
 * l’écran, on obtient les coordonnées du toucher sur l’élément
 * <video> qui affiche l’image, pas par rapport à la vidéo en
 * elle même. Cette fonction calcule les coordonnées sur l’image,
 * ce qui est utile pour savoir quel mot a été sélectionné.
 *
 * @param {Object} coordinates  Cordonnées du toucher sur l’élément <video>
 * @param {Number} zoneWidth    Largeur de l’élément <vidéo> (en pixels)
 * @param {Number} zoneHeight   Hauteur de l’élément <vidéo> (en pixels)
 * @param {Number} cameraWidth  Largeur du flux vidéo (en pixels)
 * @param {Number} cameraHeight Hauteur du flux vidéo (en pixels)
 *
 * @return {Object} Coordonnées du toucher relatives à l’image
 */
function viewPositionToCameraPosition({ x, y }, zoneWidth, zoneHeight, cameraWidth, cameraHeight) {
    // On calcule d’abord l’échelle à laquelle la vidéo est zoomée pour tenir dans l’élément <video>
    // Si scale == 2, cela veut dire que la vidéo est 2 fois plus grande que la zone.
    const scale = Math.min(cameraWidth / zoneWidth, cameraHeight / zoneHeight);

    // Calcul du nombre de pixels cachés sur les côtés gauche et droit
    let leftMargin = 0;
    if (zoneWidth * scale <= cameraWidth) {
        leftMargin = cameraWidth - (zoneWidth * scale);
    }

    // Calcul du nombre de pixels cachés sur les côtés haut et bas
    let topMargin = 0;
    if (zoneHeight * scale <= cameraHeight) {
        topMargin = cameraHeight - (zoneHeight * scale);
    }

    // Calcul final des coordonnées sur la vidéo
    return {
        x: x * scale + leftMargin / 2,
        y: y * scale + topMargin / 2,
    };
}
