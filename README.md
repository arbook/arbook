# ARBook

<p align="center"><img src="public/icon-big.png" height="256" width="256"></p>

ARBook est une application web permettant à ses utilisateurs de récupérer des définitions dans un livre en utilisant la caméra de leur smartphone.

## Installation
Cette section vous guidera pour installer une copie du projet en local pour le tester et contribuer à son développement.

### Récupération du code
Le code d’ARBook est hébergé sur GitLab. Vous pouvez cloner le repository Git ainsi :
```sh
git clone https://gitlab.com/arbook/arbook.git
```

### Installation de PHP
Pour pouvoir lancer une instance en local d’ARBook, nous vous recommandons d’installer vous-même PHP sur votre ordinateur. Cette étape est également nécessaire pour utiliser Composer, partie indispensable au fonctionnement du projet (son utilisation est expliquée en détail plus bas da).
- sous Windows : utilisez le lien de téléchargement [sur le site de PHP](https://windows.php.net/download#php-7.1)
- sous macOS : `brew install php71` (il vous sera nécessaire d’installer le gestionnaire de packages [https://brew.sh](Homebrew) au préalable)
- sous Ubuntu : `sudo apt-get install php` (pour les autres distributions Linux, utilisez le gestionnaire de packages installé)

### Installation de Composer
Comme beaucoup d’autres projets PHP, ARBook utiise Composer. Ce logiciel gère les dépendences du projet : il s’assure que le code externe nécessaire au fonctionnement d’ARBook est bien installé et à jour. Parmi les dépendances d’ARBook, on trouve notamment `google/cloud-vision` qui permet d’utiliser l’API Cloud Vision de Google.

Pour installer Composer, suivez les instructions [sur le site officiel](https://getcomposer.org/download/). Cette page vous fournira les informations nécessaires en fonction de votre système d’exploitation.

Une fois Composer installé, vous devez installer les dépendances du projet ARBook en utilisant l’outil `composer` en ligne de commandes. Voici comment faire :
```sh
# Rendez-vous dans le dossier où ARBook est cloné
cd arbook

# Lancez l’installation des dépendences
composer install
```

### Installation d’une clé d’API Google Vision
Pour que l’API Google Vision puisse être utilisée, il vous faut une clé d’API émise par Google. Vous pouvez en obtenir une sur le site _Google API Console_. Cette clé d’API prend la forme d’un fichier JSON (si vous travaillez dans l’équipe ARBook, ce fichier sera nommé sous la forme `ARBook-***********.json`).

Premièrement, il faut glisser-déposer ce fichier dans le dossier `arbook`. Ce fichier sera ignoré par Git, ce qui vous évitera de le pubiler par erreur sur le repository public.

### Création d’un fichier d’environnement
Souvent, le code PHP a besoin d’informations qui dépend de l’environnement où le projet est actuellement installé. Par exemple, sur un projet de forum en PHP, les identifiants de base de données seront différents dans l’environnement de développement et l’environnement de production.

Les variables d’environnement sont stockées dans un fichier qui s’appelle `.env` : cela permet d’ignorer ce fichier dans Git, empêchant ainsi la divulgation d’identifiants privés sur un dépôt Git public. Cela permet également de modifier les variables d’environnement sans que les modifications ne soient prises en comptes.

Dans le cadre de notre projet ARBook, nous utilisons une variable d’environnement nommée `GOOGLE_APPLICATION_CREDENTIALS_FILE` : elle permet d’indiquer au code PHP où se trouve le fichier de clé d’API Google.

Pour créer un fichier `.env` adéquat, ouvrez votre éditeur de code et faites une copie du fichier `.env.example`, que vous nommerez `.env`. Vous devrez ensuite remplacer la valeur par défaut de `GOOGLE_APPLICATION_CREDENTIALS_FILE` dans votre fichier `.env`, pour y écrire le vrai nom du fichier d’identifiants Google.

### Lancement du serveur de développement
Une fois arrivé ici, vous avez tout ce qui faut d’installé pour pouvoir utiliser ARBook ! Pour lancer un serveur qui fait tourner le projet, vous pouvez installer PHP en ligne de commande de cette manière :
```sh
# Vous aurez besoin de vous trouver dans le dossir public : ce dossier
# contient tous les fichiers auxquels les utilisateurs doivent pouvoir
# accéder. Les fichiers qui sont en dehors du dossier public ne doivent
# pas être accessibles par les utilisateurs pour des raisons de sécurité
# (par exemple, il ne faut pas qu’un utilisateur puisse obtenir notre
# clé d’API Google Vision).
cd arbook/public/

# Vous pouvez lancer le serveur de développement de PHP avec la ligne suivante.
php -S localhost:1337
```
Vous pouvez maintenant accéder à ARBook à l’adresse http://localhost:1337 !

Si vous voulez arrêter le serveur PHP, pressez simplement la combinaison de touches `CTRL + C` dans la fenêtre où vous l’avez lancé.

## Tests
Le projet ARBook contient des tests unitaires ainsi que des tests exécutés dans un navigateur (avec Laravel Dusk) pour s’assurer du bon fonctionnement de l’application.

Avant de lancer les tests, veillez à avoir un serveur PHP tournant en local sur le port 80, avec les clés d’API Google configurées.

Vous pouvez ensuite lancer les tests en tapant la commande `./vendor/bin/phpunit` dans le dossier du projet.

## Contribuer
Pour contribuer au projet ARBook, vous devez d’abord l’installer en local (voir la section *Installation* de ce document) afin de pouvoir développer la fonctionnalité et la tester.

Ensuite, vous devez créer une branche sur Git afin de travailler sur votre fonctionnalité :
```sh
# Créer une branche
git branch -b ma-super-fonctionnalite

# L’envoyer sur le repository distant (GitLab) pour que tous les
# contributeurs du projet puissent la voir
git push -u origin ma-super-fonctionnalite
```

Vous pouvez ensuite travailler sur le projet et faire des commits. Une fois votre travail accompli, créez une Merge Request sur GitLab afin que les modifications puissent être suivies par les autres membres du projet et merge vers la branche principale (*master*) :
> [➕ **Créer une Merge Request**](https://gitlab.com/arbook/arbook/merge_requests/new)