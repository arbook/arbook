<?php
use PHPUnit\Framework\TestCase;
use ARBook\WordsDetector;

final class WordsDetectorTests extends TestCase {
    public function testItDetectsWords() {
        $image = file_get_contents(__DIR__.'/figure-65.png');
        $words = WordsDetector::detectWordsIn($image);

        $this->assertInternalType('array', $words);

        $firstWord = $words[1]; // $words[0] is all the detected text
        $this->assertEquals('It', $firstWord['value']);
    }
}
