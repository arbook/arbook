<?php

namespace ARBook\Tests\Browser;

use Konsulting\DuskStandalone\TestCase;

abstract class BaseDuskTestCase extends TestCase
{
    // Set the base url for the browser requests
    protected function baseUrl()
    {
        return 'http://localhost';
    }

    // Set the path for browser tests, this is where screenshots/console logs
    // are stored. The default is [app root]/tests/Browser based on the 
    // vendor folder location as per a normal Composer install.
    protected function browserTestsPath()
    {
        return parent::browserTestsPath();
    }
}