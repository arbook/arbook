<?php

namespace ARBook\Tests\Browser;

use Laravel\Dusk\Browser;

class HomePageTests extends BaseDuskTestCase
{
    /** @test * */
    public function it_shows_the_home_page()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit('/')
                ->assertSee('AR Book')
                ->assertSee('À venir…');
        });
    }
}