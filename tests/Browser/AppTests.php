<?php

namespace ARBook\Tests\Browser;

use Laravel\Dusk\Browser;

class AppTests extends BaseDuskTestCase
{
    /** @test */
    public function it_can_detect_words()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit('/app');

            // Inject fake media device
            $data = file_get_contents(__DIR__.'/fixtures/word.png');
            $img = 'data:image/png;base64,'.base64_encode($data);
            $mockScript = 'window.mockImage = '.json_encode($img).';';
            $browser->driver->executeScript($mockScript);

            $browser->assertMissing('.photo-overlay');
            $browser->driver->executeScript('onVideoTouch(0, 0)');
            $browser->assertVisible('.photo-overlay');

            $browser->waitFor('.definition-panel', 30)
                ->assertSee('tomate')
                ->assertSee('(Botanique) Plante annuelle de la famille des solanacées, originaire d’Amérique du Sud et Centrale.');
        });
    }
}