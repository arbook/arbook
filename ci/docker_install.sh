#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install git -yqq

# Install phpunit, the tool that we will use for testing
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit

# Install and run a Selenium server
apt-get update && \
    apt-get install -y openjdk-8-jdk && \
    apt-get install -y ant && \
    apt-get clean
apt-get update && \
    apt-get install ca-certificates-java && \
    apt-get clean && \
    update-ca-certificates -f

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export PATH=$PATH:$JAVA_HOME/bin

curl --location --output /usr/local/bin/selenium.jar https://selenium-release.storage.googleapis.com/2.45/selenium-server-standalone-2.45.0.jar
java -jar /usr/local/bin/selenium.jar &

# Install mysql driver
# Here you can install any other extension that you need
# docker-php-ext-install pdo_mysql
docker-php-ext-install bcmath
docker-php-ext-install json

apt-get install -y zlib1g-dev
docker-php-ext-install zip
