<?php
namespace ARBook;

use Google\Cloud\Vision\V1\ImageAnnotatorClient;

/**
 * Détecter les mots dans une image
 */
class WordsDetector {
    /**
     * Détecte les mots dans une image
     *
     * @param string $image Contenu du fichier image
     * @return array Mots détectés
     */
    public static function detectWordsIn($image): array
    {
        // Créer le client d’API Google
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.__DIR__.'/../'.getenv('GOOGLE_APPLICATION_CREDENTIALS_FILE'));
        $client = new \Google_Client();
        $imageAnnotator = new ImageAnnotatorClient();

        // Lance la détection de texte par Google et récupère les textes détectés
        $response = $imageAnnotator->textDetection($image);
        $texts = $response->getTextAnnotations();

        // Formatter la sortie
        $words = [];

        foreach ($texts as $text) {
            $corners = [];
            $vertices = $text->getBoundingPoly()->getVertices();

            $sumX = 0;
            $sumY = 0;
            foreach ($vertices as $vertex) {
                $corners[] = [
                    'x' => $vertex->getX(),
                    'y' => $vertex->getY(),
                ];

                $sumX += $vertex->getX();
                $sumY += $vertex->getY();
            }

            $words[] = [
                'value' => $text->getDescription(),
                'corners' => $corners,
                'center' => [
                    'x' => $sumX / count($vertices),
                    'y' => $sumY / count($vertices),
                ],
            ];
        }

        $imageAnnotator->close();

        return $words;
    }
}
